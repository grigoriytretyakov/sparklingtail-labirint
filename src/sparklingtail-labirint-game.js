import 'phaser';

import LabirintScene from './LabirintScene';

const config = {
    type: Phaser.AUTO,
    parent: 'sparklingtail-labirint-game',
    width: 800,
    height: 600,
    //scaleMode: 0, //Phaser.ScaleManager.EXACT_FIT,
    backgroundColor: 0x535d6c,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 800 },
            debug: false
        }
    },
    scene: [
        LabirintScene,
    ]
};

const game = new Phaser.Game(config);
