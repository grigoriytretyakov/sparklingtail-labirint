class SparklingTail extends Phaser.GameObjects.Sprite {
    constructor() {
        this.x = 100;
        this.y = 100;
    }

    update() {
    }
};
const FRAMERATE = 10;

class LabirintScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'LabirintScene'
        });
    }

    preload() {
        this.load.image('brick', './assets/brick.png');
        this.load.image('stone', './assets/stone.png');
        this.load.spritesheet(
            'spike',
            './assets/spike.png',
            {frameWidth: 21, frameHeight: 21}
        );
        this.load.spritesheet(
            'tail',
            './assets/sparklingtail.png',
            {frameWidth: 32, frameHeight: 39}
        );
    }

    create() {
        this.sparklingtail = this.physics.add.sprite(100, 50, 'tail');
        this.sparklingtail.setBounce(0.6);
        this.sparklingtail.setCollideWorldBounds(true);
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('tail', { start: 0, end: 3 }),
            frameRate: FRAMERATE,
            repeat: -1
        });
        this.sparklingtail.anims.play('right', true);

        this.labirint = this.physics.add.staticGroup();

        for (let i = 0; i < 15; i++) {
            this.labirint.create(16 + i * 32, 300, 'brick');
        }

        this.physics.add.collider(this.sparklingtail, this.labirint);

        this.add.sprite(300, 200, 'stone');

        this.spike = this.add.sprite(170, 100, 'spike');
        this.anims.create({
            key: 'spike',
            frames: this.anims.generateFrameNumbers('spike', { start: 0, end: 1}),
            frameRate: FRAMERATE,
            repeat: -1
        });
        this.spike.anims.play('spike', true);
    }

    update(time, delta) {
        //console.log('frameRate', FRAMERATE)
    }
};

export default LabirintScene;
